﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangeBesrift
{
    public class Class
    {
        [CommandMethod("BeschriftLaenge")]
        public void laenge()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;

            String myFormat = "{0:F2}";

            PromptIntegerResult pir = ed.GetInteger("Nachkommastellen [2] : ");
            if (pir.Status == PromptStatus.OK)
            {
                myFormat = "{0:F" + pir.Value.ToString() + "}";
            }

            bool Ersetzen = false;

            PromptStringOptions pso = new PromptStringOptions("");
            pso.Message = "Trennzeichen [Komma ,] [Punkt .]:";
            pso.DefaultValue = ",";
            pso.UseDefaultValue = true;
            pso.AppendKeywordsToMessage= true;
            PromptResult pr = ed.GetString(pso);

            if (pr.Status == PromptStatus.OK)
            {
                if (pr.StringResult == "," || pr.StringResult == ".")
                {
                    if(pr.StringResult == ",")
                    {
                        Ersetzen = true;
                    }
                }
                else
                    ed.WriteMessage("\nNur Komma oder Punkt als Trennzeichen!");
            }

            PromptStringOptions psoPref = new PromptStringOptions("Prefix > ");
            psoPref.DefaultValue = "";
            PromptResult prPref = ed.GetString(psoPref);
            if (prPref.Status != PromptStatus.OK)
                return;
            String prefix = prPref.StringResult;

            PromptStringOptions psoSuff = new PromptStringOptions("Sufix > ");
            psoPref.DefaultValue = "";
            PromptResult prSuff = ed.GetString(psoSuff);
            if (prSuff.Status != PromptStatus.OK)
                return;
            String suffix = prPref.StringResult;

            String msg = "\nLinie wahlen";
            PromptEntityOptions peo = new PromptEntityOptions(msg);

            PromptEntityResult per = ed.GetEntity(peo);
            if (per.Status != PromptStatus.OK) return;
            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                Entity ent = tr.GetObject(per.ObjectId, OpenMode.ForRead) as Entity;
                String leng = "";
                String entName = ent.GetRXClass().DxfName;

                if (entName == "POLYLINE")
                {
                    Polyline3d myPoly = ent as Polyline3d;
                    leng = String.Format(myFormat, myPoly.Length);
                }
                else if (entName == "LWPOLYLINE")
                {
                    Polyline myPoly = ent as Polyline;
                    leng = String.Format(myFormat, myPoly.Length);
                }
                else if (entName == "LINE")
                {
                    Line myPoly = ent as Line;
                    leng = String.Format(myFormat, myPoly.Length);
                }
                else return;

                String ep = "\nEinfügepunkt:";
                PromptPointOptions ppo = new PromptPointOptions(ep);
                PromptPointResult ppr = ed.GetPoint(ppo);
                if (ppr.Status != PromptStatus.OK) return;
                

                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                using (MText myText = new MText())
                {
                    Matrix3d Ucs2Wcs = ed.CurrentUserCoordinateSystem;
                    Matrix3d Wcs2Ucs = Ucs2Wcs.Inverse();

                    myText.Location = ppr.Value;
                    if (Ersetzen)
                    {
                        leng = leng.Replace(".", ",");
                    }
                    myText.Contents = prefix + leng + suffix;

                    btr.AppendEntity(myText);
                    tr.AddNewlyCreatedDBObject(myText, true);

                    myText.TransformBy(Ucs2Wcs);
                }
                

                    tr.Commit();
            }
        }
    }
}
